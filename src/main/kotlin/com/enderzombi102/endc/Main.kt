package com.enderzombi102.endc

import com.enderzombi102.endc.lexer.Lexer
import com.enderzombi102.endc.tokenizer.Tokenizer
import java.io.File

fun main( argv: Array<String> ) {
	val command = Arguments.parse( argv )
	val x = Tokenizer.tokenize( File( Arguments.file ).readText(), Arguments.file )
	val y = Lexer().lex( x.first )
	println( y )
}