package com.enderzombi102.endc.util

fun <T> T?.ifNull( func: () -> T ) = this ?: func.invoke()

fun <T> T.`while`(predicate: (T) -> Boolean, block: (T) -> T ): T {
	var value = this
	while ( predicate.invoke( value ) )
		value = block.invoke( value )
	return value
}