package com.enderzombi102.endc.util

import com.enderzombi102.endc.lexer.ast.CompilationUnit

@Suppress("unused")
interface Backend {
	fun main( unit: CompilationUnit ): Int
}