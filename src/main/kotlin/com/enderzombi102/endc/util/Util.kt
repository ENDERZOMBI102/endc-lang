package com.enderzombi102.endc.util

const val NULL = '\u0000'

fun findClosestUpperBoundedDivisible( value: Int, divider: Int ): Int {
	var res = value
	while ( res % divider != 0 )
		res++
	return res
}