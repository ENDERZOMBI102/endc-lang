package com.enderzombi102.endc.util

import com.enderzombi102.endc.lexer.Lexer
import com.enderzombi102.endc.tokenizer.Token
import com.enderzombi102.endc.tokenizer.TokenType

fun Lexer.match(token: Token = consume(), func: MatchBody.() -> Unit ) {
    val body = MatchBody()
    func(body)
    for ( ( key, value ) in body.cases )
        if ( key.first == token.type && ( key.second == null || key.second == token.value ) )
            return value( token )
    body.defaultBody( token )
}

class MatchBody {
    internal val cases: MutableMap<Pair<TokenType, String?>, (Token) -> Unit> = HashMap()
    internal var defaultBody: Token.() -> Unit = { }

    fun on( type: TokenType, value: String? = null, function: Token.() -> Unit ) {
        cases[ Pair( type, value ) ] = function
    }

    fun on( vararg types: TokenType, function: Token.() -> Unit ) {
        for ( type in types )
            cases[ Pair( type, null ) ] = function
    }

    fun default( function: Token.() -> Unit ) {
        defaultBody = function
    }
}
