package com.enderzombi102.endc.util

enum class Platform( val display: String, val help: String, val available: Boolean ) {
	Dotnet(
		display = ".net",
		help = "Compiles to a CIR assembly",
		available = false
	),
	Interpreter(
		display = "Interpreter",
		help = "Directly interpret the source code",
		available = false
	),
	Llvm(
		display = "LLVM",
		help = "Compiles to LLVM IR and then to machine code",
		available = false
	),
	Wasm(
		display = "WASM",
		help = "Compiles to WASM text format and then to binary",
		available = false
	),
	Neko(
		display = "Neko VM",
		help = "Compiles to the Neko VM bytecode",
		available = false
	),
	HashLink(
		display = "HashLink",
		help = "Compiles to the HashLink bytecode",
		available = false
	),
	Jvm(
		display = "Java Virtual Machine",
		help = "Compiles to JVM bytecode (.class files in a .jar)",
		available = false
	),
	Python(
		display = "Python 3.9 VM",
		help = "Compiles to python bytecode (.pyc files)",
		available = false
	),
	Javascript(
		display = "JavaScript",
		help = "Transpiles to js code",
		available = false
	)
}