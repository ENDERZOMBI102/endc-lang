package com.enderzombi102.endc

import com.enderzombi102.endc.util.Platform
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType

object Arguments {
	fun parse( argv: Array<String> ) = parser.parse( argv )

	private val parser = ArgParser("endcc")

	val file by parser.argument(
		ArgType.String,
		"file",
		"Input file"
	)
	val backend by parser.option(
		ArgType.Choice<Platform>(),
		"backend",
		"b",
		"Backend to run after parsing"
	)
	val config by parser.option(
		ArgType.String,
		"config",
		"c",
		"Sets the config file to the provided path"
	)
	val verbosity by parser.option(
		ArgType.Choice( listOf( 0, 1, 2 ), String::toInt ),
		"verbosity",
		"v",
		"Sets the verbosity of the log (0: everything 1: warns+errors 2: errors)"
	)
	val showBackendHelp by parser.option(
		ArgType.Boolean,
		"backend-info",
		description = "Used to get information about available backends"
	)
	val interactive by parser.option(
		ArgType.Boolean,
		"interactive",
		description = "Used to run the interpreter in interactive mode"
	)
	val exitOnImplError by parser.option(
		ArgType.Boolean,
		"exit-on-error",
		description = "Makes the interpreter exits when a implementation error occurs"
	)
	val debug by parser.option(
		ArgType.Boolean,
		"debug",
		"dg",
		"Enables compiler debug mode"
	)
	val genConfig by parser.option(
		ArgType.Boolean,
		"gen-config",
		description = "Creates a default config on the path specified by --config and exits"
	)
	val logStyle by parser.option(
		ArgType.Choice<LogStyle>(),
		"log-style",
		"l",
		"Defines how errors and warnings indicate the line and character it originates from"
	)
	val exitAtStage by parser.option(
		ArgType.Choice<Stage>(),
		"exit-at-stage",
		description = "Stops execution at a given step"
	)
	val dumpTokens by parser.option(
		ArgType.Boolean,
		"dump-tokens",
		description = "Dumps the tokens generated from a file to \$FILENAME.tks"
	)
	val dumpAst by parser.option(
		ArgType.Boolean,
		"dump-ast",
		description = "Dumps the ast generated from a file to \$FILENAME.ast"
	)
}

enum class LogStyle {
	Normal,
	Compact
}

enum class Stage {
	Tokenization,
	Lexing,
	Parsing,
	Backend
}