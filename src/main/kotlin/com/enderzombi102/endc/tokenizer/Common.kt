package com.enderzombi102.endc.tokenizer

data class Loc( val file: String, val range: IntRange, val line: Int, val col: Int )

enum class TokenType {
	NAME,
	FLOAT,
	SYMBOL,
	NEWLINE,
	COMMENT,
	EOF,
	STRING,
	KEYWORD
}

data class Token( val type: TokenType, val value: String, val loc: Loc ) {
	override fun toString() = when ( type ) {
		TokenType.FLOAT -> "float value $value"
		TokenType.STRING -> "string value '$value'"
		else -> "$type $value"
	}
}

interface Error { val message: String; val loc: Loc }
data class TokenizerError( override val message: String, override val loc: Loc ) : Error
data class LexerError( override val message: String, override val loc: Loc ) : Error