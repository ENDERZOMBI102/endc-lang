package com.enderzombi102.endc.tokenizer

import com.enderzombi102.endc.util.findClosestUpperBoundedDivisible

@Suppress("DEPRECATION")
object Tokenizer {
	private val BACKENDS = listOf( "DOTNET", "LLVM", "WASM", "NKO", "HASHLINK", "JVM", "PYTHON", "JAVASCRIPT" )
	private val KEYWORDS = listOf( "IS", "ORTHRWIS", "FUTHRMOR", "IF", "LS", "DO", "CHCK", "UNTIL", "WHN", "FINISHD", "DCLAR", "CONSTANT", "VARIABL", "GIV", "BACK", "SUBROUTIN", "CALL", "XPORT", "TMPLAT", "BHAVIOR", "BUILD", "OWN", "FROM", "INITIALIZR", "DINITIALIZR", "SCTION", "ASM", "M", "NO", "NOTHING" )
	private const val SYMBOLS = "{|}()<>,.[]$/:=+ඞ;-;<>"
	private val LETTERS = Regex("[a-zA-Z_$]")

	fun tokenize( code: String, file: String ): Pair<List<Token>, List<Error>> {
		val middleman = rawTokenize( code, file )
		val res = matureTokens( middleman.first )
		return res to middleman.second
	}

	private fun rawTokenize(code: String, file: String ): Pair<List<RawToken>, List<Error>> {
		var i = 0
		var line = 1
		var col = 1
		var startPos = 0

		val tokens = mutableListOf<RawToken>()
		val errors = mutableListOf<TokenizerError>()

		fun token(type: RawTokenType, value: String, loc: Loc = Loc( file, startPos .. i, line, col ) ) {
			tokens.add( RawToken( type, value, loc ) )
			if ( type == RawTokenType.NEWLINE ) {
				line++
				col = 1
			} else
				col += value.length
		}
		fun error( message: String ) =
			errors.add( TokenizerError( message, Loc( file, startPos .. i, line, col ) ) )

		while (i < code.length) {
			val char = code[i]
			startPos = i

			when {
				char == '|' && code[i + 1] == '*' -> {
					var found = false
					var comment = ""
					i++
					while ( i < code.length ) {
						found = code.substring( i .. i + 1 ) == "*|"
						if ( found )
							break
						comment += code[i++]
					}
					if (! found )
						error( "Reached end of file while looking for end of multiline comment!" )
					i += 2
					if (! comment.contains('\n') )
						error( "Encountered end of comment on same line!" )
					token( RawTokenType.COMMENT, comment.strip() )
					line += comment.count { it == '\n' }
					col = comment.lines().last().length + 2
				}

				char in LETTERS -> {
					var string = ""

					while ( i < code.length && ( code[i] in LETTERS || char.isDigit() ) )
						string += code[i++]
					i--

					token( RawTokenType.NAME, string )
					col += string.length - 1
				}

				char == '*' -> {
					var string = ""
					i++

					while ( i < code.length && code[i] != '*' ) {
						when ( val character = code[i] ) {
							'\\' -> string += when ( val nextChar = code[++i] ) {
								'*' -> '*'
								'r' -> "\n"
								'n' -> error("Cannot use `\\n` in strings!")
								else -> "\\$nextChar"
							}
							else -> string += character
						}
						i++
					}

					token( RawTokenType.STRING, string )
					col += i - startPos
				}

				char.isDigit() || ( char == ',' && code[i + 1].isDigit() )-> {
					var numberString = if ( code[i + 1].isDigit() ) "0" else ""
					var decimal = false
					var errored = false

					while ( code[i].isDigit() || code[i] in ".," ) {
						if ( errored || code[i] == '.' ) {
							i++
							continue
						}

						if ( code[i] == ',' ) {
							if (decimal) {
								error("Cannot have multiple decimal points in a number!")
								errored = true
							}
							decimal = true
						}

						numberString += code[ i++ ]
					}
					i--

					token( RawTokenType.NUMBER, numberString )
					col += i - startPos
				}

				char == '\t' -> {
					var count = 1
					while ( code[++i] == '\t' )
						count++
					i--
					if ( count % 2 != 0 )
						error( "Found $count tabs instead of ${findClosestUpperBoundedDivisible(count, 3)}!" )

					val end = code.indexOf( '\n', i ) - 1
					token( RawTokenType.COMMENT, code.substring( i .. end ) )
					i = end
				}

				char == '\n' -> {
					token( RawTokenType.NEWLINE, "\\n" )

					var count = 0
					while ( code[++i] == ' ' )
						count++
					i--

					if ( count % 5 != 0 )
						error( "Line starts with $count spaces, instead of ${findClosestUpperBoundedDivisible( count, 5 )}" )
				}

				char in SYMBOLS -> token( RawTokenType.SYMBOL, char.toString() )
				char == ' ' -> col++
			}
			i++
		}

		return tokens to errors
	}

	private fun matureTokens( rawTokens: List<RawToken> ): List<Token> {
		var i = 0

		val tokens = mutableListOf<Token>()

		fun mature( type: TokenType, tok: RawToken ) = tokens.add( Token( type, tok.value, tok.loc ) )
		fun mature( type: TokenType, value: String, loc: Loc ) = tokens.add( Token( type, value, loc ) )

		do {
			val token = rawTokens[i]
			val  loc = token.loc
			when ( token.type ) {
				RawTokenType.NUMBER -> mature( TokenType.FLOAT, token )
				RawTokenType.STRING -> mature( TokenType.STRING, token )
				RawTokenType.NAME -> mature(
					when ( token.value ) {
						in KEYWORDS -> TokenType.KEYWORD
//						in BACKENDS -> TokenType.BACKEND  // TODO: Figure out if this is a good idea
						else -> TokenType.NAME
					},
					token
				)
				RawTokenType.SYMBOL -> when ( token.value ) {
					"$" -> when ( rawTokens[++i].value ) {
						"[" -> mature( TokenType.SYMBOL, "$[", loc.copy( range = loc.range.first .. loc.range.last + 1 ) )
						else -> {
							i--

						}
					}
					"]" -> when ( rawTokens[++i].value ) {
						"$" -> mature( TokenType.SYMBOL, "]$", loc.copy( range = loc.range.first .. loc.range.last + 1 ) )
						else -> {
							i--
							mature( TokenType.SYMBOL, token )
						}
					}
					"-" -> when ( rawTokens[++i].value ) {
						">" -> mature( TokenType.SYMBOL, "->", loc.copy( range = loc.range.first .. loc.range.last + 1 ) )
						else -> {
							i--
							mature( TokenType.SYMBOL, token )
						}
					}
					"<" -> when ( rawTokens[++i].value ) {
						"-" -> mature( TokenType.SYMBOL, "<-", loc.copy( range = loc.range.first .. loc.range.last + 1 ) )
						else -> {
							i--
							mature( TokenType.SYMBOL, token )
						}
					}
					"+" -> when ( rawTokens[++i].value ) {
						"+" -> mature( TokenType.SYMBOL, "++", loc.copy( range = loc.range.first .. loc.range.last + 1 ) )
						else -> {
							i--
							mature( TokenType.SYMBOL, token )
						}
					}
					"=" -> when ( rawTokens[++i].value ) {
						"<" -> mature( TokenType.SYMBOL, "=<", loc.copy( range = loc.range.first .. loc.range.last + 1 ) )
						else -> {
							i--
							mature( TokenType.SYMBOL, token )
						}
					}
					"(", ")", "{", "}", "/", ":", ",", "ඞ", ";", ";", "." -> mature( TokenType.SYMBOL, token )
				}
				RawTokenType.NEWLINE -> mature( TokenType.NEWLINE, token )
				RawTokenType.COMMENT -> mature( TokenType.COMMENT, token )
			}
		} while ( ++i < rawTokens.size )

		val last = tokens.last().loc
		mature( TokenType.EOF, "", Loc( last.file, tokens.size .. tokens.size, last.line + 1, 0 ) )

		return tokens
	}
}

private operator fun Regex.contains( char: Char ) = this.matches( "$char" )

private data class RawToken( val type: RawTokenType, val value: String, val loc: Loc )

private enum class RawTokenType {
	NAME,
	NUMBER,
	SYMBOL,
	NEWLINE,
	COMMENT,
	STRING
}