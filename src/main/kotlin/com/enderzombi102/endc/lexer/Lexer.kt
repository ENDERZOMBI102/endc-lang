package com.enderzombi102.endc.lexer

import com.enderzombi102.endc.lexer.ast.*
import com.enderzombi102.endc.tokenizer.LexerError
import com.enderzombi102.endc.tokenizer.Token
import com.enderzombi102.endc.tokenizer.TokenType
import com.enderzombi102.endc.util.match


@Suppress("SameParameterValue")
class Lexer {
    private lateinit var tokens: List<Token>
    private lateinit var errors: MutableList<LexerError>
    private var i: Int = 0

    // region util
    private fun peek(offset: Int = 1) = tokens[i + offset]
    private fun atEof() = tokens[if (i >= tokens.size) tokens.size - 1 else i].type == TokenType.EOF
    internal fun consume() = tokens[i++]
    override fun toString() = "Lexer{ i=$i, tokens=$tokens }"

    private fun peekIs(type: TokenType, vararg values: String) =
        peek(0).type == type && (values.isEmpty() || peek(0).value in values)

    private fun peekIs(type: TokenType, ignoreNewlines: Boolean = false, vararg values: String): Boolean {
        var offset = 0

        if (ignoreNewlines)
            while (peek(offset).type == TokenType.NEWLINE)
                offset += 1

        return peek(offset).type == type && (values.isEmpty() || peek(offset).value in values)
    }

    private fun error(message: String) = errors.add( LexerError( message, peek().loc.copy() ) )

    private fun consumeOrNull(err: String, value: String? = null, vararg types: TokenType): Token? {
        if (peek(0).type in types && (value == null || peek(0).value == value))
            return consume()
        else
            error(err)
        return null
    }

    private fun consumeOrNull(err: String, types: TokenType, vararg values: String): Token? {
        if (peek(0).type == types && (values.isEmpty() || peek(0).value in values))
            return consume()
        else
            error(err)
        return null
    }

    private fun consumeIfIs(type: TokenType, value: String): Boolean {
        if (peekIs(type, value)) {
            consume()
            return true
        }
        return false
    }

    private fun consumeIfIs(type: TokenType): Boolean {
        if (peekIs(type)) {
            consume()
            return true
        }
        return false
    }
    // endregion util

    @Suppress("unused", "ControlFlowWithEmptyBody")
    fun lex(tokens: List<Token>): CompilationUnit {
        val imports = ArrayList<ImportClause>()
        val globals = HashMap<String, Variable>()
        val subroutines = HashMap<String, Subroutine>()
        val templates = HashMap<String, Template>()

        this.tokens = tokens.filter { it.type != TokenType.COMMENT } // for now, remove comments
        this.i = 0

        fun parseDeclaration( exported: Boolean ) {
            match {
                on( TokenType.KEYWORD, "SUBROUTIN" ) {
                    val func = parseSubroutine( exported )
                    subroutines[func.name] = func
                }
                on( TokenType.KEYWORD, "TMPLAT" ) {
                    val func = parseTemplate( exported )
                    subroutines[func.name] = func
                }
                on( TokenType.KEYWORD, "CONSTANT" ) {
                    i--
                    val constant = parseVariable( exported )
                    globals[constant.name] = constant
                }
                on( TokenType.KEYWORD, "VARIABL" ) {
                    i--
                    val variable = parseVariable( exported )
                    globals[variable.name] = variable
                }
                default {
                    error( "Expected keyword [SUBROUTIN|TMPLAT|CONSTANT|VARIABL] after XPORT, found ${peek(0)}" )
                }
            }
        }

        do {
            // remove all newlines
            while ( consumeIfIs(TokenType.NEWLINE) );

            match {
                on(TokenType.KEYWORD, "XPORT") {
                    if ( consumeIfIs( TokenType.KEYWORD, "DCLAR" ) )
                        parseDeclaration( true )
                    else
                        error( "Expected keyword DCLAR after XPORT, found ${peek(0)}" )
                }
                on(TokenType.KEYWORD, "DCLAR") {
                    parseDeclaration( false )
                }
                on(TokenType.KEYWORD, "OWN") {
                    if ( globals.isNotEmpty() || templates.isNotEmpty() || subroutines.isNotEmpty() )
                        error("Imports can only happen on top of a file.")
                    val import = parseImport()
                    imports.add( import )
                }
                on(TokenType.EOF) { }
                default {
                    error("Expected `func`, `type` or `var` keywords, got $this")
                }
            }
        } while (!atEof())

        return CompilationUnit( Block(arrayListOf()), globals, templates, subroutines )
    }

    /**
     * Parsers an import statement
     * ```
     * OWN grt. pat FROM importable/
     * ```
     */
    private fun parseImport(): ImportClause {
        consumeOrNull("Expected `path` after `imp` keyword!", TokenType.STRING)

    }

    /**
     * Parsers a function declaration
     * ```
     * XPORT DCLAR SUBROUTIN pat{StRiNg nam} <- StRiNg [
     *      DCLAR CONSTANT StRiNg patTxt____ = * is patting the dog.* /
     *      GIV BACK nam - patTxt____/
     * ]
     * ```
     * NOTE: `SUBROUTIN` has already been removed before this is called!
     */
    private fun parseSubroutine(exported: Boolean = false): Subroutine {
        // check if the function name is specified
        // Name
        val name =
            consumeOrNull("Expected `name` after `func` keyword in function declaration!", TokenType.NAME)?.value

        // (
        consumeOrNull("Expected `{` symbol after `name` in function declaration!", "{", TokenType.SYMBOL)

        // StRiNg nam
        val params = mutableListOf<Variable>()
        while (i + 1 < tokens.size && peekIs(TokenType.NAME)) {
            params.add(parseParameter())

            if (peekIs(TokenType.NAME))
                error("Expected `)` or `,` symbols after `param` in function declaration!")

            consumeIfIs(TokenType.SYMBOL, ",")
        }

        // )
        consumeOrNull("Expected `)` symbol after `params` in function declaration!", ")", TokenType.SYMBOL)

        // : i32
        val type = if (consumeIfIs(TokenType.SYMBOL, ":"))
            parseTypeReference("function")
        else
            VoidType.Void

        // <body>
        val body = parseBlock()

        return Subroutine( exported, name, params, type, body )
    }

    /**
     * Parsers a function parameter declaration
     * ```
     * StRiNg() argv______
     * ```
     */
    private fun parseParameter(): Variable {
        // Type
        val type = parseTypeReference()
        // Name
        val name = consumeOrNull("Expected `name` after `func` keyword in function declaration!", TokenType.NAME)?.value

    }

    /**
     * Parsers a block declaration
     * ```
     * [
     *      DCLAR CONSTANT StRiNg patTxt____ = * is patting the dog.* /
     *      GIV BACK nam - patTxt____/
     * ]
     * ```
     */
    private fun parseBlock(): Block {
        consumeOrNull("Expected `{` symbol to start a block!", "{", TokenType.SYMBOL)

        // -> paramName.int + 12
        val statements = ArrayList<Statement>()
        while (i + 1 < tokens.size && !peekIs(TokenType.SYMBOL, true, "}")) {
            statements.add(parseStatement())
            consumeOrNull("Expected `newline` after `statement` in block!", TokenType.NEWLINE)
        }
        consumeIfIs(TokenType.NEWLINE)
        consumeIfIs(TokenType.NEWLINE)

        consumeOrNull("Expected `}` symbol after `body` in block!", "}", TokenType.SYMBOL)

        return Block(statements)
    }

    /**
     * Parsers a variable declaration
     *
     * ```
     * DCLAR CONSTANT StRiNg tsttmplat_ = *hello world! * /
     * DCLAR VARIABL  StRiNg tsttmpla__ = *hllo world! * /
     * ```
     *
     * NOTE: `DCLAR` has already been removed before this is called!
     */
    private fun parseVariable( exported: Boolean = false ): Variable {
        val constant = when {
            consumeIfIs(TokenType.KEYWORD, "CONSTANT") -> true
            consumeIfIs(TokenType.KEYWORD, "VARIABL") -> false
            else -> {
                error("Expected keyword [CONSTANT|VARRIABL] after DCLAR keyword.")
                false
            }
        }
        // bar
        val name = consumeOrNull("Expected `name` for variable declaration!", TokenType.NAME)?.value
        // :
        consumeOrNull("Expected `:` symbol after `name` in variable declaration!", ":", TokenType.SYMBOL)
        // u1
        val type = parseTypeReference("variable")

        return Variable(
            exported,
            constant,
            Identifier( name!! ),
            type,
            if (consumeIfIs(TokenType.SYMBOL, "="))
                parseExpression()!!
            else
                null
        )
    }

    private fun parseTypeReference(where: String) = BaseType.findType(
        consumeOrNull(
            "Expected `name` or `primitive` after `:` symbol in $where declaration!",
            null,
            TokenType.PRIMITIVE,
            TokenType.NAME
        ).value
    )

    /**
     * Parsers an identifier/name
     * ```
     * name.a.f
     * ```
     */
    private fun parseIdentifier(): Identifier =
        Identifier(
            consume().value,
            if (consumeIfIs(TokenType.SYMBOL, "."))
                parseIdentifier()
            else
                null
        )

    /**
     * Parsers a type structure
     * ```
     * type Name [
     *   foo: i64
     *   bar: u1
     * ]
     * ```
     * NOTE: `type` has already been removed before this is called!
     */
    private fun parseTemplate(exported: Boolean): DefinedType {
        // Name
        val name = consumeOrNull("Expected `name` after `type` keyword!", TokenType.NAME).value
        // [
        consumeOrNull("Expected `[` symbol after `name` in type declaration!", "[", TokenType.SYMBOL)
        // \n
        consumeIfIs(TokenType.NEWLINE)
        // variables
        val vars = ArrayList<DefinedVariable>()
        while (i + 1 < tokens.size && peekIs(TokenType.NAME)) {
            vars.add(parseVariable(exported1 = exported))
            consumeOrNull("Expected `newline` after `var` declaration", TokenType.NEWLINE)
        }
        // ]
        consumeOrNull("Expected `]` symbol after `newline` in type declaration!", "]", TokenType.SYMBOL)

        return DefinedType(name, vars)
    }

    /**
     * Parsers a statement
     * ```
     * -> paramName.int + 12
     * ```
     */
    private fun parseStatement(): Statement {
        var statement: Statement = ExpressionStatement(LiteralExpression(""))
        consumeIfIs(TokenType.NEWLINE)
        // Check if token is a keyword, a name, or return, else throw error
        match {
            on(TokenType.KEYWORD, "var") {
                statement = VarDefStatement(parseVariable(exported1 = exported))
            }
            on(TokenType.KEYWORD, "if") {
                statement = parseIfChain()
            }
            on(TokenType.NAME) {
                statement = parseAssign()
            }
            on(TokenType.SYMBOL, "->") {
                statement = ReturnStatement(parseExpression())
            }
            on(TokenType.KEYWORD, "for") {
                statement = parseForLoop()
            }
            on(TokenType.KEYWORD, "while") {
                statement = parseWhileLoop()
            }
            on(TokenType.KEYWORD, "do") {
                statement = parseDoWhileLoop()
            }
            default {
                error("Expected keyword, name, or a return; but got '$this'")
            }
        }
        return statement
    }

    // region statements
    /**
     * Parsers a while loop
     * ```
     * while cond {
     *   // code
     * }
     * ```
     */
    private fun parseWhileLoop(): Statement {
        // cond
        val condition = parseExpression()
        val block = parseBlock()

        return WhileStatement(condition, block)
    }

    /**
     * Parsers a do-while loop
     * ```
     * do {
     *   // code
     * } while cond
     * ```
     */
    private fun parseDoWhileLoop(): Statement {
        // cond
        val block = parseBlock()
        consumeOrNull("Expected `while` keyword after `}` in do-while loop!", "while", TokenType.KEYWORD)
        val condition = parseExpression()

        return DoWhileStatement(condition, block)
    }

    /**
     * Parsers an if/else chain
     * ```
     * for var i: i32 = 1, i < max, i++ {
     *    num = TestType[i num].add()
     * }
     * ```
     */
    private fun parseForLoop(): Statement {
        val variable = parseVariable(exported1 = exported)
        consumeOrNull("Expected `,` symbol after `vardef` in for loop!", ",", TokenType.SYMBOL)
        val condition = parseExpression()
        consumeOrNull("Expected `,` symbol after `condition` in for loop!", ",", TokenType.SYMBOL)
        val operation = parseExpression()
        val block = parseBlock()

        return ForStatement(
            variable,
            condition,
            operation,
            block
        )
    }

    /**
     * Parsers an if/else chain
     * ```
     * if number == 0 {
     *   -> 1
     * } [else statement|block]
     * ```
     */
    private fun parseIfChain(): Statement {
        val cond = parseExpression()
        val block = parseBlock()

        return IfStatement(
            cond,
            block,
            if (consumeIfIs(TokenType.KEYWORD, "else"))
                if (peekIs(TokenType.SYMBOL, "{"))
                    ElseStatement(parseBlock())  // if else-block
                else
                    parseStatement() // if else-statement
            else
                null  // if
        )
    }

    /**
     * Parsers an assignment
     * ```
     * paramName.int = 12
     * ```
     */
    private fun parseAssign(): Statement {
        i--
        val id = parseIdentifier()
        consumeOrNull("Expected `=` symbol after `name` in assign statement!", "=", TokenType.SYMBOL)
        return AssignStatement(id, parseExpression())
    }
    // endregion statements

    /**
     * Parsers an expression
     * ```
     * paramName.int + 12
     * ```
     */
    private fun parseExpression() = equality()

    // region expressions
    private fun equality(): Expression {
        var expr = comparison()

        while (peekIs(TokenType.SYMBOL, "==", "!="))
            expr = BinaryExpression(expr, Operator.of(consume().value), comparison())

        return expr
    }

    private fun comparison(): Expression {
        var expr = term()

        while (peekIs(TokenType.SYMBOL, ">", ">=", "<", "<=", "&&", "||"))
            expr = BinaryExpression(expr, Operator.of(consume().value), term())

        return expr
    }

    private fun term(): Expression {
        var expr = factor()

        while (peekIs(TokenType.SYMBOL, "-", "+"))
            expr = BinaryExpression(expr, Operator.of(consume().value), factor())

        return expr
    }

    private fun factor(): Expression {
        var expr = access()

        while (peekIs(TokenType.SYMBOL, "/", "*", "%"))
            expr = BinaryExpression(expr, Operator.of(consume().value), access())

        return expr
    }

    private fun access(): Expression {
        var expr = unary()

        while (consumeIfIs(TokenType.SYMBOL, "."))
            expr = BinaryExpression(expr, Operator.ACC, unary())

        return expr
    }

    private fun unary(): Expression =
        if (peekIs(TokenType.SYMBOL, "!", "-"))
            UnaryExpression(Operator.of(consume().value), unary())
        else if (peekIs(TokenType.SYMBOL, "--", "++"))
            UnaryExpression(Operator.of(consume().value), call())
        else {
            val expr = call()
            if (peekIs(TokenType.SYMBOL, "--", "++"))
                InverseUnaryExpression(expr, Operator.of(consume().value))
            else
                expr
        }

    private fun call(): Expression {
        var expression: Expression = primary()

        while (true) {
            expression = if (peekIs(TokenType.SYMBOL, "("))
                finishCall((expression as NamedExpression).name)
            else if (peekIs(TokenType.SYMBOL, "["))
                finishConstruct((expression as NamedExpression).name)
            else
                break
        }

        return expression
    }

    private fun finishCall(name: Identifier): Expression {
        val arguments = ArrayList<Expression>()

        if (consumeIfIs(TokenType.SYMBOL, "(")) {
            while (peekIs(TokenType.NAME) || consumeIfIs(TokenType.SYMBOL, ",")) {
                arguments.add(parseExpression())
            }
        }

        consumeOrNull("Expect ')' after arguments.", TokenType.SYMBOL, ")")
        return CallExpression(NamedExpression(name), arguments)
    }

    private fun finishConstruct(name: Identifier): Expression {
        val arguments = ArrayList<Expression>()

        if (consumeIfIs(TokenType.SYMBOL, "[")) {
            do {
                arguments.add(parseExpression())
            } while (!peekIs(TokenType.SYMBOL, "]"))
        }

        consumeOrNull("Expect ']' after arguments.", TokenType.SYMBOL, "]")
        return ConstructExpression(name, arguments)
    }

    private fun primary(): Expression {
        var expression: Expression = LiteralExpression("")

        match {
            on(TokenType.FLOAT, TokenType.STRING) {
                expression = LiteralExpression(value)
            }
            on(TokenType.SYMBOL, "(") {
                val expr = equality()
                consumeOrNull("Expected `)` symbol after `expr` in grouped expression!", ")", TokenType.SYMBOL)
                expression = GroupedExpression(expr)
            }
            on(TokenType.NAME) {
                i-- // needed as match always consumes a token
                val id = parseIdentifier()
                expression = NamedExpression(id)
            }
            default {
                error("Expected expression, got $this..")
            }
        }

        return expression
    }
    // endregion expressions
}
