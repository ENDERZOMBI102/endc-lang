package com.enderzombi102.endc.lexer.ast

sealed interface Expression

data class LiteralExpression( val value: String ) : Expression

data class UnaryExpression(val op: Operator, val right: Expression ) : Expression
data class InverseUnaryExpression( val left: Expression, val op: Operator ) : Expression

data class BinaryExpression( val left: Expression, val op: Operator, val right: Expression ) : Expression

data class GroupedExpression( val right: Expression ) : Expression

data class CallExpression( val func: Expression, val params: List<Expression> ) : Expression
data class ConstructExpression( val name: Identifier, val params: List<Expression> ) : Expression

data class NamedExpression( val name: Identifier ) : Expression

data class AnonymousSubroutineExpression(val params: List<Variable>, val retType: Type, val body: Block ) : Expression
