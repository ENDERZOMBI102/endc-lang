package com.enderzombi102.endc.lexer.ast

data class CompilationUnit(
	val body: Block,
	val globals: Map<String, Variable>,
	val templates: Map<String, Template>,
	val subroutines: Map<String, Subroutine>,
) {
	val exportedGlobals get() = globals.filter { it.value.exported }
	val exportedTemplates get() = templates.filter { it.value.exported }
	val exportedSubroutines get() = subroutines.filter { it.value.exported }
}

data class Template(
	val exported: Boolean,
	val name: String,
	val initializer: Subroutine,
	val behaviors: List<Subroutine>,
	val finalizer: Subroutine
)

data class Variable( val exported: Boolean, val constant: Boolean, val name: String, val type: Type, val expr: Expression )


data class Subroutine(
	val exported: Boolean,
	val name: String,
	val params: List<Variable>,
	val retType: Type,
	val body: Block
)

data class Block( val statements: List<Statement> )
