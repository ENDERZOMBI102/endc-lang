package com.enderzombi102.endc.lexer.ast

sealed interface Statement

data class ExpressionStatement( val expr: Expression ) : Statement

data class VarDefStatement( val def: Variable ) : Statement

data class ReturnStatement( val expr: Expression ) : Statement

data class IfStatement( val condition: Expression, val body: Block, val next: Statement ) : Statement
data class ElseStatement( val body: Block ) : Statement

data class DoStatement( val condition: Expression, val body: Block, val finishBody: Block ) : Statement
data class UntilStatement( val condition: Expression, val body: Block, val finishBody: Block ) : Statement
