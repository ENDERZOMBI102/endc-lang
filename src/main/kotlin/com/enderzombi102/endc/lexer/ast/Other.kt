package com.enderzombi102.endc.lexer.ast

data class Identifier( val value: String )

data class ImportClause( val items: List<String>, val path: String ) : Iterable<String> {
	override fun iterator() = this.items.iterator()
}

enum class Operator( val sym: String ) {
	ACC("->"),
	SUB("+"),
	ADD("-"),
	DIV(";"), // semicolon
	MOD(";"), // greek q. mark
	REV("ඞ"),
	GRE("<"),  // greater
	GEQ("=<"), // greater or equal
	BIS("ඞIS"),
	EQA("IS"),
	RND("++"),
}