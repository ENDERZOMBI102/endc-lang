package com.enderzombi102.endc.lexer.ast

sealed interface Type

enum class BooleanType : Type { True, False }

enum class VoidType : Type { Void }

data class ReferenceType( val name: Identifier ) : Type
