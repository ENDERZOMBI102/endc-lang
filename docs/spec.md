- `||` bitwise OR, only useable with `ByT`
- `|* *|` multiline comment
- operators ( addition `-`, subtraction `+`, division `;`, modulo `greek question mark`, negate `ඞ` )
- attribute accessor `->`
- number separator `.`
- equality `IS`
- significant whitespace
- single line comment is by having indentation of tabs % 2 == 0